from __future__ import annotations

import abc
import pygame as game
import pygame.event as event


class Scene(metaclass=abc.ABCMeta):
    _sceneManager: SceneManager = None

    def setSceneManager(self, sceneManager: SceneManager) -> None:
        self._sceneManager = sceneManager
        return

    @abc.abstractmethod
    def init(self, surface: game.Surface) -> None: return

    @abc.abstractmethod
    def render(self, surface: game.Surface, dT: float) -> None: pass

    @abc.abstractmethod
    def handleEvent(self, event: event.Event) -> None: pass

class SceneManager:
    _stack: list[str]
    _scenes: dict[str,list[Scene,bool]] = {}
    _surface: game.Surface = None

    def __init__(self, defaultStack: list[str]) -> None:
        self._stack = defaultStack
        pass

    def register(self,name: str, scene: Scene) -> None:
        scene.setSceneManager(self)
        self._scenes[name] = [scene, False]
        pass

    def push(self, name: str) -> None:
        self._stack.append(name)
        if not self._surface == None:
            self.init(self._surface)
        pass

    def pushAndReplace(self, name:str) -> None:
        del self._stack[-1]
        self._stack.append(name)
        if not self._surface == None:
            self.init(self._surface)
        pass

    def pop(self) -> None:
        del self._stack[-1]
        if not self._surface == None:
            self.init(self._surface)
        pass
    
    def _checkSel(self):
        if len(self._scenes) == 0:
            raise RuntimeError("Rendered before registering any Scenes")
        elif self._stack[-1] not in self._scenes:
            raise KeyError(f"Unknown scene \"{self._stack[-1]}\"")

    def init(self, surface: game.Surface) -> None:
        self._checkSel()
        self._surface = surface
        if not self._scenes[self._stack[-1]][1]:
            self._scenes[self._stack[-1]][0].init(surface)
        pass

    def render(self, surface: game.Surface, dT: float) -> None:
        self._checkSel()
        self._scenes[self._stack[-1]][0].render(surface, dT)
        pass

    def handleEvent(self, event: event.Event) -> None:
        self._checkSel()
        self._scenes[self._stack[-1]][0].handleEvent(event)
        pass
        
