import pygame as game


class PingPongBall:

    def __init__(self, position: tuple[int, int], size: int = 20, speed: int = 5, color: tuple[int, int, int] = (0xFF, 0x00, 0x00), dirV: tuple[int, int] = (5, 1)):
        self.position = position
        self.size = size
        self.speed = speed
        self.color = color
        self.dirV = dirV
        pass

    def __setattr__(self, name: str, value) -> None:
        if name == "dirV":
            inv_val = (value[0] ** 2 + value[1] ** 2) ** -0.5
            value = (value[0] * inv_val, value[1] * inv_val)
            pass
        super(PingPongBall, self).__setattr__(name, value)
        pass

    def render(self,  surface: game.Surface, dT: float):
        canvasSize = surface.get_size()
        self.obj = game.draw.circle(surface, self.color, (
            self.position[0] + self.size / 2, self.position[1] + self.size / 2), self.size / 2)

        self.position = (self.position[0] + self.dirV[0] * self.speed *
                         dT, self.position[1] + self.dirV[1] * self.speed * dT)

        if self.position[0] < 0:
            self.dirV = (self.dirV[0] * -1, self.dirV[1])
            self.position = (0, self.position[1])
            pass
        elif self.position[1] < 0:
            self.dirV = (self.dirV[0], self.dirV[1] * -1)
            self.position = (self.position[0], 0)
            pass
        elif self.position[0] + self.size > canvasSize[0]:
            self.dirV = (self.dirV[0] * -1, self.dirV[1])
            self.position = (canvasSize[0] - self.size, self.position[1])
            pass
        elif self.position[1] + self.size > canvasSize[1]:
            self.dirV = (self.dirV[0], self.dirV[1] * -1)
            self.position = (self.position[0], canvasSize[1] - self.size)
            pass
        pass
