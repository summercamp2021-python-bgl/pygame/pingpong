import pygame as game
from enum import Enum, unique

from pygame.display import update
from gameElements.ball import PingPongBall


class PingPongBatSide(Enum):
    TOP = 1
    BOTTOM = 2
    LEFT = 3
    RIGHT = 4


class PingPongBat:
    _reInit: bool = False
    _movementV: tuple[int, int] = (0, 0)
    _position: tuple[int, int] = (0, 0)
    _size: tuple[int, int] = (0, 0)

    _initialized: bool = False

    def __init__(self, distance: int = 20, margin: int = 0, width: int = 200, thickness: int = 10, side: PingPongBatSide = PingPongBatSide.BOTTOM, color: tuple[int, int, int] = (0x00, 0x00, 0xFF), speed: int = 50):
        self.distance = distance
        self.margin = margin
        self.width = width
        self.thickness = thickness
        self.side = side
        self.color = color
        self.speed = speed
        pass

    def __setattr__(self, name: str, value) -> None:
        if name == "side" or \
                name == "margin" or \
                name == "width" or \
                name == "thickness" or \
                name == "distance":
            self._reInit = True
            pass
        super(PingPongBat, self).__setattr__(name, value)
        pass

    def init(self, surface: game.Surface) -> None:
        self._initialized = True
        coords = surface.get_size()
        if self.side == PingPongBatSide.TOP:
            self._position = ((coords[0] - self.width) // 2, self.distance)
            self._size = (self.width, self.thickness)
            pass
        elif self.side == PingPongBatSide.BOTTOM:
            self._position = (
                (coords[0] - self.width) // 2, coords[1] - self.distance - self.thickness)
            self._size = (self.width, self.thickness)
            pass
        elif self.side == PingPongBatSide.LEFT:
            self._position = (self.distance, (coords[1] - self.width) // 2)
            self._size = (self.thickness, self.width)
            pass
        else:
            self._position = (coords[0] - self.distance -
                              self.thickness, (coords[1] - self.width) // 2)
            self._size = (self.thickness, self.width)
            pass
        pass

    def render(self, surface: game.Surface, dT: float):
        surfaceSize = surface.get_size()
        if not self._initialized:
            raise RuntimeError("Rendered before initialized")
        if self._reInit:
            self.init(surface)
            self._reInit = False
            pass

        if self.side == PingPongBatSide.TOP or \
                self.side == PingPongBatSide.BOTTOM:
            if self._position[0] < self.margin and self._movementV[0] == -1:
                self._movementV = (0, self._movementV[1])
                pass
            elif (self._position[0] + self.width) > (surfaceSize[0] - self.margin) and self._movementV[0] == 1:
                self._movementV = (0, self._movementV[1])
                pass
            pass
        else:
            if self._position[1] < self.margin and self._movementV[1] == -1:
                self._movementV = (self._movementV[0],0)
                pass
            elif (self._position[1] + self.width) > (surfaceSize[1] - self.margin) and self._movementV[1] == 1:
                self._movementV = (self._movementV[0],0)
                pass
            pass

        self._position = (self._position[0] + self._movementV[0] * self.speed *
                              dT, self._position[1] + self._movementV[1] * self.speed * dT)

        self.obj = game.draw.rect(surface, self.color, game.Rect(
            self._position[0], self._position[1], self._size[0], self._size[1]))
        pass

    def handleKeypress(self, event: game.event.Event, keys={"up": game.K_UP, "down": game.K_DOWN, "right": game.K_RIGHT, "left": game.K_LEFT}):
        if event.type == game.KEYDOWN:
            if self.side == PingPongBatSide.LEFT or self.side == PingPongBatSide.RIGHT:
                if event.key == keys["up"]:
                    self._movementV = (self._movementV[0], -1)
                    pass
                elif event.key == keys["down"]:
                    self._movementV = (self._movementV[0], 1)
                    pass
                pass
            else:
                if event.key == keys["left"]:
                    self._movementV = (-1, self._movementV[1])
                    pass
                elif event.key == keys["right"]:
                    self._movementV = (1, self._movementV[1])
                    pass
                pass
            pass
        elif event.type == game.KEYUP:
            if self.side == PingPongBatSide.LEFT or self.side == PingPongBatSide.RIGHT:
                if event.key == keys["up"]:
                    self._movementV = (self._movementV[0], 0)
                    pass
                elif event.key == keys["down"]:
                    self._movementV = (self._movementV[0], 0)
                    pass
                pass
            else:
                if event.key == keys["left"]:
                    self._movementV = (0, self._movementV[1])
                    pass
                elif event.key == keys["right"]:
                    self._movementV = (0, self._movementV[1])
                    pass
                pass
            pass
        pass

    def handleCollision(self, ball: PingPongBall) -> None:
        if ball.obj.colliderect(self.obj):
            if self.side == PingPongBatSide.LEFT:
                ball.dirV = (ball.dirV[0] * -1, ball.dirV[1])
                ball.position = (
                    self._position[0] + self._size[0], ball.position[1])
                pass
            elif self.side == PingPongBatSide.RIGHT:
                ball.dirV = (ball.dirV[0] * -1, ball.dirV[1])
                ball.position = (
                    self._position[0] - ball.size, ball.position[1])
                pass
            elif self.side == PingPongBatSide.TOP:
                ball.dirV = (ball.dirV[0], ball.dirV[1] * -1)
                ball.position = (
                    ball.position[0], self._position[1] + self._size[1])
                pass
            else:
                ball.dirV = (ball.dirV[0], ball.dirV[1] * -1)
                ball.position = (
                    ball.position[0], self._position[1] - ball.size)
                pass
            pass
        pass
