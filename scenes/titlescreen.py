from scene import Scene
import pygame as game
import pygame.event as event

BACKGROUND_COLOR: tuple[int, int, int] = (0xAA, 0xAA, 0xFF)


class TitleScreenScene(Scene):
    def init(self, surface: game.Surface) -> None:
        return super().init(surface)
    def render(self, surface: game.Surface, dT: float) -> None:
        surface.fill(BACKGROUND_COLOR)
        return super().render(surface, dT)
    def handleEvent(self, event: event.Event) -> None:
        if event.type == game.KEYDOWN:
            if event.key == game.K_n:
                self._sceneManager.pushAndReplace("game")
        return super().handleEvent(event)
