import pygame as game
import pygame.event as event

from scene import Scene
from gameElements.ball import PingPongBall
from gameElements.bat import PingPongBat, PingPongBatSide
from constants import WINDOW_SIZE

from random import randint, random

BACKGROUND_COLOR: tuple[int, int, int] = (0xFF, 0x00, 0x00)
NUM_BALLS = 20

class GameScene(Scene):
    bat1 = PingPongBat(
        20,
        20,
        200,
        10,
        PingPongBatSide.LEFT,
        (0x00, 0x00, 0xFF),
        500
    )
    bat2 = PingPongBat(
        20,
        20,
        200,
        10,
        PingPongBatSide.RIGHT,
        (0x00, 0xFF, 0xFF),
        500
    )

    bats = [bat1, bat2]
    balls = []

    def __init__(self) -> None:
        for _ in range(NUM_BALLS):
            self.balls.append(PingPongBall(
                (WINDOW_SIZE[0] // 2, WINDOW_SIZE[1] // 2),  # initial position
                20,                                         # diameter
                randint(50,500),                                        # speed in pixels/sec
                (0xFF, 0xAB, 0x00),                         # color
                # direction vector
                # (will be divided by the value of the vector)
                ((random() * 2) -1, (random() * 2) -1)
            ))
        super().__init__()

    def handleEvent(self, event: event.Event) -> None:
        self.bat1.handleKeypress(event)
        self.bat2.handleKeypress(
            event, {"up": game.K_w, "down": game.K_s, "left": game.K_a, "right": game.K_d})
        return super().handleEvent(event)

    def init(self, surface: game.Surface) -> None:
        self.bat1.init(surface)
        self.bat2.init(surface)
        return super().init(surface)

    def render(self, surface: game.Surface, dT: float) -> None:
        surface.fill(BACKGROUND_COLOR)

        for bat in self.bats:
            bat.render(surface, dT)
            pass
        for ball in self.balls:
            ball.render(surface, dT)
            for bat in self.bats:
                bat.handleCollision(ball)
                pass
            pass

        return super().render(surface, dT)
    pass
