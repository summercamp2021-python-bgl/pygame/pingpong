from scenes.titlescreen import TitleScreenScene
from scenes.game import GameScene
from scene import SceneManager
import pygame as game
import pygame.event as event
import pygame.display as display
import pygame.time as time
from constants import *

running: bool = True

sceneManager: SceneManager = None

def main() -> None:
    game.init()
    surface: game.Surface = display.set_mode(WINDOW_SIZE)
    display.set_caption(WINDOW_TITLE)
    clock = time.Clock()

    global sceneManager
    sceneManager = SceneManager(["titlescreen"])
    sceneManager.register("titlescreen", TitleScreenScene())
    sceneManager.register("game",GameScene())

    sceneManager.init(surface)

    frameTime = 0
    while running:
        for e in event.get():
            handle_event(e)
            pass
        display.flip()
        frameTime = clock.tick(MAX_FPS)
        sceneManager.render(surface, frameTime / 1000)
        pass

    game.quit()
    pass


def handle_event(event: event.Event) -> None:
    global running
    if event.type == game.QUIT or \
            (event.type == game.KEYDOWN and
             event.key == game.K_ESCAPE):
        running = False
        pass
    sceneManager.handleEvent(event)
    pass

main()
